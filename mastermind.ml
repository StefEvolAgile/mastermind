(** module Mastermind principal, avec IHM générique passée en paramètre *)

open Code
open Ia
open Jeu
open Ihm_texte
 
module Mastermind (Ihm: Ihm_type) = struct
    (** verifie_reponses : jeu -> bool *)
    let verifie_reponses jeu = List.for_all2 (fun x y -> Code.reponse jeu.code_cache x = Some y) jeu.codes_tentes jeu.reponses 
    (** mastermind_interne : jeu -> unit *)
    let mastermind_interne jeu =
        Random.self_init ();
        let partie_gagnee_oracle jeu =
            let rec tentative jeu =
                let code_tente =
                    if Jeu.devin_humain jeu then Ihm.saisie_code_tente () 
                    else Ia.choix (Jeu.methode_ia_devin jeu) jeu.codes_tentes jeu.codes_possibles
                in let jeu = {jeu with codes_tentes = code_tente::jeu.codes_tentes}
                in Ihm.affichage_tentative jeu;
                let rep = 
                    if Jeu.oracle_humain jeu && not jeu.reponse_auto then Ihm.saisie_reponse () 
                    else Code.reponse jeu.code_cache code_tente 
                in match rep with 
                |None -> false (* TODO *)
                |Some (a, b) ->
                    let jeu = {jeu with reponses = (a, b)::jeu.reponses}
                    in Ihm.affichage_reponse jeu;
                    if a = Code.nombre_pions then false
                    (** Avant de conclure que l'oracle a gagné, on vérifie les reponses*)
                    else if List.length jeu.codes_tentes >= jeu.max_tentative then verifie_reponses jeu
                    else tentative {jeu with codes_possibles = Ia.filtre 0 (code_tente, Some (a, b)) jeu.codes_possibles}
            in  tentative jeu
        in let rec alterne jeu =
            Ihm.affichage_debut_partie jeu;
            let jeu = {jeu with 
                code_cache = if Jeu.oracle_humain jeu then Ihm.saisie_code_cache () else Ia.code_aleatoire Code.tous;
                codes_tentes = []; reponses = []; codes_possibles = Code.tous
            }
            in let oracle_gagne = partie_gagnee_oracle jeu
            in let joueur1_gagne = (oracle_gagne && Jeu.joueur1_oracle jeu) || ((not oracle_gagne) && (not (Jeu.joueur1_oracle jeu)))
            in let jeu = {jeu with 
                score = (fst jeu.score + (if joueur1_gagne then 1 else 0), snd jeu.score  + (if joueur1_gagne then 0 else 1));
                numero_partie = jeu.numero_partie + 1;
            }
            in Ihm.affichage_fin_partie jeu joueur1_gagne;
            if jeu.numero_partie <= jeu.nombre_parties then alterne jeu
            else Ihm.affichage_fin_jeu jeu
        in Ihm.affichage_debut_jeu jeu; 
        alterne {jeu with numero_partie = 1; nombre_parties = (jeu.nombre_parties + 1) / 2 * 2} (* nombre de parties paires *)

    (** mastermind : string -> int -> int -> bool -> unit *)
    let mastermind nom_joueur max_tentative nombre_parties reponse_auto =
        mastermind_interne 
            { (Jeu.init ()) with
                    nom_joueur1 = nom_joueur; methode_ia_joueur1 = None;
                    nom_joueur2 = "HAL"; methode_ia_joueur2 = Some 0;
                    max_tentative; nombre_parties; reponse_auto;
            }

    (** mode wizard : choix du type de jeu par l'IHM *)
    let mastermind_wizard _ = mastermind_interne (Ihm.choix_jeu ())
end

module Mastermind_texte = Mastermind(Ihm_texte);;
(* module Mastermind_graphique = Mastermind(Ihm_graphique);; *)

Mastermind_texte.mastermind_wizard ()
(* Mastermind_texte.mastermind "Stéphane" 10 4 true *) (* mode EvH *)
