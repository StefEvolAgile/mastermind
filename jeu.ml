(** Définition d'un jeu de mastermind et module d'interface avec l'IHM *)

open Code

type jeu = {
    (** paramètres du jeu *)
    nom_joueur1 : string;
    methode_ia_joueur1 : int option; (** None pour humain *)
    nom_joueur2 : string;
    methode_ia_joueur2 : int option;
    max_tentative : int;
    nombre_parties : int;
    reponse_auto : bool;

    (** variables du jeu *)
    score : int * int;
    numero_partie : int;

    (** variables de la partie en cours *)
    code_cache : Code.t;
    codes_tentes : Code.t list;
    reponses : (int * int) list;
    codes_possibles : Code.t list;
}

(** init: unit -> jeu : initialise un jeu *)
let init () = {nom_joueur1 = ""; methode_ia_joueur1 = None; nom_joueur2 = ""; methode_ia_joueur2 = None;
    max_tentative=0; nombre_parties = 0; reponse_auto = false;
    score = (0, 0); numero_partie = 0; code_cache = []; codes_tentes=[]; reponses = [];
    codes_possibles = []}


(** fonctions utiles *)
let joueur1_oracle jeu = (jeu.numero_partie mod 2) = 1 (* partie 1 : joueur 1 oracle, puis alternance *)
let oracle_humain jeu = if joueur1_oracle jeu then (jeu.methode_ia_joueur1 = None) else (jeu.methode_ia_joueur2 = None)
let devin_humain jeu = if joueur1_oracle jeu then (jeu.methode_ia_joueur2 = None) else (jeu.methode_ia_joueur1 = None)
let methode_ia_devin jeu = 
    let m = if joueur1_oracle jeu then jeu.methode_ia_joueur2 else jeu.methode_ia_joueur1 in
    match m with 
    | Some x -> x
    | None -> 0  (* Cas non prévu *)

(** signature d'un module d'interface homme-machine générique *)
module type Ihm_type = sig
    val affichage_debut_jeu : jeu -> unit (* affiche un nouveau jeu *)
    val affichage_debut_partie : jeu -> unit (* affiche une nouvelle partie *)
    val affichage_tentative : jeu -> unit (* affiche une nouvelle tentative *)
    val affichage_reponse : jeu -> unit (* affiche une nouvelle réponse *)
    val affichage_fin_partie : jeu -> bool -> unit  (* affiche la fin d'une partie pour un jeu donné, et booléen joueur1_gagnant *)
    val affichage_fin_jeu : jeu -> unit (* fin du jeu *)
    val saisie_code_cache : unit -> Code.t  (* saisie d'un code caché *)
    val saisie_code_tente : unit -> Code.t  (* saisie d'un code tenté *)
    val saisie_reponse : unit -> (int * int) option (* saisie d'une réponse *)
    val choix_jeu : unit -> jeu (* renvoie un jeu avec les paramètres initialisés *)
end
