(** Algorithmes de recherche de code par ordinateur *)
open Code;;

module Ia :
sig

(** Nombre d'algorithmes developpes *)
val nombre_methodes : int

(** Choisit un code a proposer
* @param methode 0 pour l'algorithme naif,
* 1 pour l'algorithme de KNUTH
* ... et ainsi de suite
* @param essais la liste des codes deja proposes
* @param possibles la liste des codes possibles
* @return le prochain code a essayer
*)
val choix : int -> Code.t list -> Code.t list -> Code.t

(** Filtre les codes possibles
* @param methode 0 pour l'algorithme naif,
* 1 pour l'algorithme de KNUTH
* ... et ainsi de suite
* @param (code, rep) le code essaye et la reponse correspondante
* @param la liste de courante de codes possibles
* @param la nouvelle liste de codes possibles
*)
val filtre : int -> (Code.t * (int * int) option) -> Code.t list -> Code.t list

(** choisit un code aléatoirement dans une liste *)
val code_aleatoire : Code.t list -> Code.t

end =
struct

let nombre_methodes = 3
let max_list l = List.fold_left max 0 l    

let code_aleatoire l = List.nth l (Random.int (List.length l))

let filtre methode (code, rep) courante = List.filter (fun x -> Code.reponse x code = rep) courante 

let choix methode essais possibles = 
    (* Printf.printf "methode %d - %d possibilités\n" methode (List.length possibles); flush stdout; *)
    match methode with
    | 0 -> (** Methode naive *)
        begin
            match possibles with
            | [] -> code_aleatoire Code.tous (** pas de bonne réponse => réponse aléatoire *)
            | _ -> List.hd possibles 
        end
    | 1 -> 
        (** Methode Knuth: algorithme du minimax *)
        (** On joue le coup par défaut au premier tour (1122[...2]) pour gagner un peu de temps *)
        (** Mais ça fonctionne aussi si on laisse l'IA faire le calcul, c'est juste un peu plus long *)
        if List.length essais = 0 then (List.init Code.nombre_pions (fun x -> if x < 2 then 1 else 2)) else
        let essais_ponderes = 
            (** On calcule d'abord pour chaque essai envisageable son pire score en le confrontant à tous les possibles *)
            (** Le pire score étant le max de la taille de possibles après filtrage *)
            List.map 
                (fun e -> (e, max_list (List.map (fun x -> List.length (filtre methode (e, Code.reponse x e) possibles)) possibles)))
                Code.tous 
        (** On choisit ensuite le meilleur coup envisageable en prenant le meilleur (=min) pire (=max) score (d'où le nom minimax) *)
        (** C'est donc le coup qui fait prendre le moins de risque possible *)
        (** A noter qu'on peut être amené à jouer un coup "impossible" si son score est le meilleur *)
        (** Toutefois, on préfère à score égal une solution possible, sans cela l'algorithme peut échouer *)
        in let classement = 
            (List.sort 
                (fun (a,b) (c, d) -> match (compare b d) with
                    | 0 -> if List.mem a possibles then -1 else 1
                    | x -> x
                )
                essais_ponderes
            )
        in begin
            match classement with
            | [] -> code_aleatoire Code.tous (** pas de bonne réponse => réponse aléatoire *)
            | _ -> fst (List.hd classement)
        end 
    | _ ->  (** Methode random *)
        begin
            match possibles with
            | [] -> code_aleatoire Code.tous (** pas de bonne réponse => réponse aléatoire *)
            | _ -> code_aleatoire possibles
        end
end ;;