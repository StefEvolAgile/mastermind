(**  Module Code: définition des codes pour le jeu du mastermind *)

module Code :
sig
    val nombre_couleurs : int
    type pion = int (** nombre entre 1 et nombre de couleurs *)
    type t = pion list
    val nombre_pions : int
    val couleurs_possibles : pion list
    val compare : t -> t -> int
    val string_of_pion : pion -> string

    val string_of_code : pion list -> string

    val code_of_string : string -> t option

    (** La liste de tous les codes permis *)
    val tous : t list
    (** La liste de toutes les reponses possibles *)
    val toutes_reponses : (int * int) list ;;

    (** Calcule la reponse d'un code par rapport au code cache
    * @param code le code propose
    * @param vrai_code le code cache
    * @return un couple (nombre de pions bien places, nombre de pions mal places)
    [None] si la reponse ne peut etre calculee
    *)
    val reponse : t -> t -> (int * int) option
    val autotest : bool
end =
struct
let nombre_couleurs = 6
type pion = int
type t = pion list
let nombre_pions = 4
let couleurs_possibles = List.init nombre_couleurs (fun x -> x + 1) 

let string_of_pion = function 
| 1 -> "Bleu "
| 2 -> "Vert "
| 3 -> "Rouge"
| 4 -> "Blanc"
| 5 -> "Jaune"
| 6 -> "Noir "
| x -> "C" ^ string_of_int x

(** Compare deux codes
@param code1 premier code a comparer
@param code2 second  code a comparer
@return 0 si les deux codes sont identiques,
un entier positif si [code1] est strictement plus grand que [code2]
un entier negatif si [code1] est strictement plus petit que [code2]
*)

let compare code1 code2 = Pervasives.compare code1 code2;;

let string_of_code coden = List.fold_left (fun s a -> let s' = string_of_pion a in if s = "" then s' else s ^ " | " ^ s') "" coden;;

(** Conversion chaine de caracteres vers code (pour saisie)
* @param string chaine de caractere saisie
* @return le code correspondant a la saisie si la conversion est possible
[None] si la conversion n'est pas possible
val code_of_string : string -> t option*)

let code_of_string s = 
    let s = Str.global_replace (Str.regexp " ") "" s 
    in if String.length s != nombre_pions then None else
    if not (Str.string_match (Str.regexp ("^[1-" ^ (string_of_int nombre_couleurs)  ^ "]")) s 0) then None else
    Some (List.init (String.length s) (fun x -> int_of_char (String.get s x) - int_of_char '0'))

let tous = 
    let nb_couleur = List.length couleurs_possibles
    in let rec aux n accu =
        if n = nb_couleur * nb_couleur * nb_couleur* nb_couleur then accu 
        else let d = n mod nb_couleur 
            and c = n / nb_couleur mod nb_couleur
            and b = n / nb_couleur / nb_couleur mod nb_couleur
            and a = n / nb_couleur / nb_couleur / nb_couleur mod nb_couleur
            in aux (n+1) ([List.nth couleurs_possibles a; List.nth couleurs_possibles b;
                List.nth couleurs_possibles c; List.nth couleurs_possibles d]::accu)   
    in aux 0 []

let reponse code_cache code_tente = 
    let (bonne_place, reste_code_cache, reste_code_tente) = List.fold_left2 
        (fun (score, reste_code_cache, reste_code_tente) a b -> 
            if a = b then (score + 1, reste_code_cache, reste_code_tente) else (score, a::reste_code_cache, b::reste_code_tente))
                (* on calcule le reste à l'envers, mais ça ne change pas le résultat et c'est plus performant *)
        (0, [], []) code_cache code_tente
    in let (bonne_couleur, _) = List.fold_left 
        (fun (score, reste_code_tente) pion_cache ->
            let rec aux accu = function
                | [] -> (score, accu)
                | x::y -> if pion_cache = x then (score+1, accu@y) else aux (x::accu) y
            in aux [] reste_code_tente
        )
        (0, reste_code_tente) reste_code_cache
    in Some (bonne_place, bonne_couleur);;

let toutes_reponses =
     let rec aux a b accu =
        if a = (nombre_pions +1) then accu else 
        let (b', r) = (if (b+1) = (nombre_pions-a+1) then (0, 1) else (b+1, 0))
        in let a' = a+r 
        in aux a' b' ((a, b)::accu)
    in aux 0 0 []

let autotest = reponse [1; 2; 1; 2] [2; 1; 2; 1] = Some (0, 4);;

end ;; 
