(** interface homme-machine de type texte *)
open Code
open Jeu

module Ihm_texte : Ihm_type = struct
    let affichage_debut_jeu jeu =
        let string_of_type_joueur = function 
        | None -> "humain"
        | Some x -> "IA niveau " ^ (string_of_int x)
        in Printf.printf "Bienvenue dans le jeu du Mastermind !\n";
        Printf.printf "Aujourd'hui %s (%s) affronte %s (%s) !\n" 
            jeu.nom_joueur1 (string_of_type_joueur jeu.methode_ia_joueur1)
            jeu.nom_joueur2 (string_of_type_joueur jeu.methode_ia_joueur2);
        Printf.printf "Le jeu se déroulera en %d parties de %d tours maximum\n" jeu.nombre_parties jeu.max_tentative;
        flush stdout

    let affichage_debut_partie jeu = 
        Printf.printf "---------------------------------------------------\n";
        Printf.printf "Partie %d : oracle %s contre devin %s\n"
            jeu.numero_partie 
            (if Jeu.joueur1_oracle jeu then jeu.nom_joueur1 else jeu.nom_joueur2)
            (if Jeu.joueur1_oracle jeu then jeu.nom_joueur2 else jeu.nom_joueur1);
        Printf.printf "---------------------------------------------------\n";
        flush stdout

    let affichage_tentative jeu = 
        if Jeu.oracle_humain jeu && not jeu.reponse_auto then
        begin
            Printf.printf "Code caché : %s\n" (Code.string_of_code jeu.code_cache);
            Printf.printf "Tentative  : %s\n" (Code.string_of_code (List.hd jeu.codes_tentes))
        end
        else ()

    let affichage_reponse jeu =
        let l = if not (Jeu.devin_humain jeu) then [(List.length jeu.reponses, List.hd jeu.codes_tentes, List.hd jeu.reponses)] else
            List.mapi (fun x (a, b) -> (x+1, a, b)) (List.rev_map2 (fun x y -> (x, y)) jeu.codes_tentes jeu.reponses)
        in List.iter 
            (fun (n, c, (a, b)) -> 
                Printf.printf "Tentative %2d : %s - réponse : (%d, %d)\n" n (Code.string_of_code c) a b;
                flush stdout
            ) 
        l

    let affichage_fin_partie jeu joueur1_gagnant =
        Printf.printf "Code caché   : %s\n" (Code.string_of_code jeu.code_cache);
        Printf.printf "%s gagne !\n" (if joueur1_gagnant then jeu.nom_joueur1 else jeu.nom_joueur2);
        Printf.printf "Score final : %s : %d points - %s : %d points\n" jeu.nom_joueur1 (fst jeu.score) jeu.nom_joueur2 (snd jeu.score);
        flush stdout

    let affichage_fin_jeu jeu = 
        Printf.printf "********** Fin du jeu *************\n";
        match jeu.score with
        | (a,b) when a > b -> Printf.printf "%s gagne contre %s !\n" jeu.nom_joueur1 jeu.nom_joueur2;
        | (a,b) when a < b -> Printf.printf "%s gagne contre %s !\n" jeu.nom_joueur2 jeu.nom_joueur1;
        | (a,b) -> Printf.printf "Match nul entre %s et %s !\n" jeu.nom_joueur1 jeu.nom_joueur2
  
    let rec lit_int prompt a b = 
        Printf.printf "%s [%d-%d]: " prompt a b;
        match read_int_opt () with 
        | Some x when x >= a && x <= b -> x
        | _ -> lit_int prompt a b
  
    let rec saisie_code prompt =
        Printf.printf "    ** %s: [ " prompt;
        List.iter (fun x -> Printf.printf "%s%d=%s" (if x=1 then "" else "| ") x (Code.string_of_pion x)) Code.couleurs_possibles;
        Printf.printf "] ";

        let s = read_line () in match Code.code_of_string s with
        | None -> saisie_code prompt
        | Some c -> c

    let saisie_code_cache _ = saisie_code "Entrez votre code caché"
    
    let saisie_code_tente _ = saisie_code "code tenté"

    let saisie_reponse _ = 
        let bien_places = lit_int "bien placés" 0 Code.nombre_pions 
        in let mal_places = lit_int " mal placés" 0 Code.nombre_pions
        in Some (bien_places, mal_places)


    let rec lit_nom n =
        Printf.printf "Nom joueur %d : " n; 
        let s = read_line () in if s = "" then lit_nom n else s

    let rec lit_type_joueur n =
        Printf.printf "type joueur %d (-1 humain, 0+ niveau IA): " n; 
        match read_int_opt () with 
        | Some x when x = -1 -> None
        | Some x when x >= 0 -> Some x
        | _ -> lit_type_joueur n



    let rec lit_bool prompt =
        Printf.printf "%s [o / n]: " prompt;
        match  read_line () with
        | "o" -> true
        | "n" -> false
        |_ -> lit_bool prompt

    let choix_jeu _ = 
        let nom_joueur1 = lit_nom 1 and methode_ia_joueur1 = lit_type_joueur 1
        and nom_joueur2 = lit_nom 2 and methode_ia_joueur2 = lit_type_joueur 2
        and max_tentative = lit_int "Max tentatives" 1 20
        and nombre_parties = lit_int "Nombre de parties" 2 100
        in let reponse_auto = if methode_ia_joueur1 <> None && methode_ia_joueur2 <> None then true else lit_bool "Réponses automatiques"
        in { (Jeu.init ()) with nom_joueur1; methode_ia_joueur1; nom_joueur2; methode_ia_joueur2;
            max_tentative; nombre_parties; reponse_auto;
        }
end
